
# Setup
## Docker & Docker-compose
Follow these links:
* https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
* https://docs.docker.com/compose/install/

Add your user to the docker group :
```
sudo usermod -aG docker $USER
```

You should had an alias for `docker-compose` as `d-c` in the `~/.bashrc` file

**Reboot !**

## tmuxinator

```
sudo apt install tmux
gem install tmuxinator
```
You should had an alias for `tmuxinator` as `mux` in `~/.bashrc`.

A good tmux config:
https://github.com/gpakosz/.tmux#installation

If you prefer, uncomment this line in `.tmux.conf.local` :
```
# start with mouse mode enabled
set -g mouse on
```

## Git submodules
```
git submodule update --init --recursive // Gonna clone repos
```

# Start
## Docker
In one terminal, at the root folder `ImageProcessing-docker/`
```
d-c build    // One time only
d-c up       // When you want to start (keep it alive)
```

## tmuxinator
In another terminal, at the root folder `ImageProcessing-docker/`
```
tmux kill-session   // Kill older sessions

mux start project 1 // If you need to "npm i" in all nodejs servers
mux start project   // Else
mux                 // ^ shortcut
```
   
If your pane got *disconnect* from the container :
```
sh run.sh server_name 1 // If you need to npm i
sh run.sh server_name   // Else

// Example
sh run.sh upload           // For Upload
sh run.sh workers           // For Workers
```

# .env
Must have a .env file at the root folder `ImageProcessing-docker/`.
```
NODE_ENV=development | production
... // TODO Complete the list
```

# Simulated deploy for prod
Since we can't deploy to prod right now, there is a simulated version using docker-compose similar to dev.

There is a .envProd **to define** in `ImageProcessing-docker/` and act the same as way as the .env files for dev. So **make sure to define** env variables like `NODE_ENV=production` inside it.

When you think the system is ready for deployment you can start the build process with these commands:
```
// TERM 1
    d-c up
    // If everything worked (sync with TERM 2)
    Ctrl + C // Stop d-c
    cd /build
    d-c up

// TERM 2
    sh build.sh
    // If everything worked
    cd build
    mux project start 1 // For the first start so it can npm i
    mux                 // else
```

# Bash scripts
There is some bash scripts to help commun tasks.

All of them are run in `ImageProcessing-docker/` if not specified.

Some of them must be run in a tmux pane  :
```
0 ImageProcessing-upload
1 ImageProcessing-workers
```

## run.sh
Start nodemon for a specific server.

**Must be run in the tmux pane !**

`sh run.sh` - Connect to the container and start nodemon

`sh run.sh 1` - Same, but npm i before starting nodemon

`runProd.sh` is the same file but for a prod environment. Should only use this one inside `build/` where you will find a copy.

Keep in mind if you quit nodemon, you get deconnect from the container. You can use the next script if you don't want this behavior.

## connect.sh
Some time you want to be able to connect to the docker instant of a server.

**Must be run in the tmux pane !**

`sh connect.sh` - Open bash in the container associated to the tmux pane \#

## build.sh
As descriped in the `Simulated deploy for prod` section, it build the app in `build/`. For more details looks at the section. 
