#!/bin/bash
# Open bash in one of the server

# Define the server from pane number
case $TMUX_PANE in
    %0)
        SERVER="upload"
    ;;
    %1)
        SERVER="workers"
    ;;
esac

# Connect
docker exec -it "imageprocessingdocker_imageprocessing-${SERVER}_1" bash -c "pkill node; cd /var/projetS5/service; bash"
