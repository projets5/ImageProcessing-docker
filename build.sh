# #!/bin/bash
# # Build all servers and bundle everthing into build/

# # # Coleo-api
# # echo "==== coleo-api ==="
# # docker exec -it coleodocker_coleo-api_1 bash -c "cd /var/coleo/server; gulp clean; gulp;"
# # echo "==================\n"

# # # Coleo-app
# # echo "==== coleo-app ==="
# # docker exec -it coleodocker_coleo-app_1 bash -c "cd /var/coleo/server; gulp clean; gulp;"
# # echo "==================\n"


# # # Coleo-media
# # echo "==== coleo-media ==="
# # docker exec -it coleodocker_coleo-media_1 bash -c "cd /var/coleo/server; gulp clean; gulp;"
# # echo "====================\n"

# echo "==== Gulps ==="
# docker exec coleodocker_coleo-api_1 bash -c "cd /var/coleo/server; gulp clean; gulp;" &
# docker exec coleodocker_coleo-app_1 bash -c "cd /var/coleo/server; gulp clean; gulp;" &
# docker exec coleodocker_coleo-media_1 bash -c "cd /var/coleo/server; gulp clean; gulp;" &
# wait
# echo "==============\n"


# # Create & populate build folder
# echo "==== Building... ==="
# rm -rf ./build
# mkdir build

# mkdir build/coleo-api
# cp -r coleo-api/build/* build/coleo-api

# mkdir build/coleo-app
# cp -r coleo-app/build/* build/coleo-app

# mkdir build/coleo-media
# cp -r coleo-media/build/* build/coleo-media

# cp -r internal_modules build/
# cp -r nginx build/
# cp -r postgres build/

# cp .envProd build/.env
# cp docker-compose.yml build/
# cp shared.nodejs.Dockerfile build/
# cp runProd.sh build/
# cp .tmuxinator.prod.yml build/.tmuxinator.yml
# # echo "==================== \n"

# echo "DONE"