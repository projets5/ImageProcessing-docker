#!/bin/bash
# Execute nodemon for one of the server

# Define the server from pane number
case $TMUX_PANE in
    %0)
        SERVER="imageprocessingdocker_imageprocessing-upload_1"
    ;;
    %1)
        SERVER="imageprocessingdocker_imageprocessing-workers_1"
    ;;
    %2)
        SERVER="imageprocessingdocker_ctrlcenter_1"
    ;;
esac

# Setup npm or not
if [ -z $1 ]; then
    # Dont
    docker exec -it "${SERVER}" bash -c "pkill node; cd /var/projetS5/service; nodemon;"
else
    # Do
    docker exec -it "${SERVER}" bash -c "pkill node; cd /var/projetS5/service; npm i; chmod 777 -R .; nodemon;"
fi
