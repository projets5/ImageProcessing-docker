# #!/bin/bash
# # Execute nodemon for one of the server

# # Define the server from pane number
# case $TMUX_PANE in
#     %0)
#         SERVER="app"
#     ;;
#     %1)
#         SERVER="api"
#     ;;
#     %2)
#         SERVER="media"
#     ;;
# esac

# # Setup npm or not
# if [ -z $1 ]; then
#     # Dont
#     docker exec -it "build_coleo-${SERVER}_1" bash -c "pkill node; cd /var/coleo/server; nodemon server/server.js;"
# else
#     # Do
#     docker exec -it "build_coleo-${SERVER}_1" bash -c "pkill node; cd /var/coleo/server; npm i; chmod 777 -R .; nodemon server/server.js;"
# fi